/**
    breakout_tester
    breakout_tester.ino
    Purpose: Sketch to test the breakout PCB for CryoWerx SATS

    @author Eliot Lim (github: @eliotlim)
    @version 0.0.1 (19/9/17)
*/

#include <Arduino.h>

#include "config.h"

void setup() {
    // Initialize the Serial port
    Serial.begin(9600);

    // Print Firmware Version
    Serial.print(PREFIX_STRING);
    Serial.print(" - v");
    Serial.println(VERSION_STRING);

    // Setup Pin Modes
    pinMode(LED_R, OUTPUT);
    pinMode(LED_G, OUTPUT);
    pinMode(LED_B, OUTPUT);
    pinMode(LOCK_OPEN, OUTPUT);
    pinMode(LOCK_FEEDBACK, INPUT);

    // Setup Pin Levels
    digitalWrite(LOCK_OPEN, LOW);
    digitalWrite(LOCK_FEEDBACK, HIGH);
    digitalWrite(LED_R, LOW);
    digitalWrite(LED_G, LOW);
    digitalWrite(LED_B, LOW);

    Serial.println("Setup Complete.");
}

void loop() {
    Serial.println("Testing LEDs");
    delay(1000);

    // Cycle through LED Strip colours
    digitalWrite(LED_R, HIGH);
    delay(LED_CYCLE_MS);
    digitalWrite(LED_R, LOW);
    digitalWrite(LED_G, HIGH);
    delay(LED_CYCLE_MS);
    digitalWrite(LED_G, LOW);
    digitalWrite(LED_B, HIGH);
    delay(LED_CYCLE_MS);
    digitalWrite(LED_B, LOW);

    Serial.println("Testing Door Lock");
    delay(1000);

    // Open Door Command
    Serial.println("[DOOR] Open");
    digitalWrite(LOCK_OPEN, HIGH);
    digitalWrite(LED_G, HIGH);
    delay(3000);
    digitalWrite(LOCK_OPEN, LOW);

    // Wait for door to close
    Serial.println("[DOOR] Lock Command Sent");
    int ledState = HIGH;
    // Wait for total 30 seconds
    for (int i = 0; i < 27; i++) {
        // Blink Green LED
        ledState = (ledState == HIGH ? LOW : HIGH);
        digitalWrite(LED_G, ledState);
        // Detect Door Locked Event
        if (digitalRead(LOCK_FEEDBACK) == LOW) {
            Serial.println("\n[DOOR] Locked");
            break;
        } else {
            Serial.print(".");
        }
        delay(1000);
    }
    digitalWrite(LED_G, LOW);

    // Simulate RFID Reading
    Serial.println("\nSimulating RFID");
    digitalWrite(LED_R, HIGH);
    delay(5000);
    digitalWrite(LED_R, LOW);

    // Completed testing cycle
    Serial.println("Test Routine Complete");
    digitalWrite(LED_R, HIGH);
    digitalWrite(LED_G, HIGH);
    delay(3000);

    digitalWrite(LED_R, LOW);
    digitalWrite(LED_G, LOW);
    digitalWrite(LED_B, LOW);

}
