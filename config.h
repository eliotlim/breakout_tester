/**
    breakout_tester
    config.h
    Purpose: Provide common configuration options and global definitions

    @author Eliot Lim (github: @eliotlim)
    @version 0.0.1 (19/9/17)
*/

#ifndef CONFIG_H
#define CONFIG_H

/**********************************
 * Static Information
 **********************************/
#define PREFIX_STRING "breakout_tester"
#define VERSION_STRING "0.0.1"

/**********************************
 * Pin Compatibility Mappings
 **********************************/
#define LED_PM0 9
#define LED_PM1 10
#define LED_PM2 11
#define LOCK_PK4 12
#define LOCK_PK5 13

/**********************************
 * Pin Mappings
 **********************************/
#define LED_R LED_PM1
#define LED_G LED_PM0
#define LED_B LED_PM2
#define LOCK_OPEN LOCK_PK4
#define LOCK_FEEDBACK LOCK_PK5

/**********************************
 * Configuration Options
 **********************************/
#define LED_CYCLE_MS 3000

#endif
